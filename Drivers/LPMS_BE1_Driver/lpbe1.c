/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#include "lpbe1.h"
#include "lpbe1_driver.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

    
    
/* Follow 3 functions are low level drivers depend on platforms
  * must be complement by user in file "lpbe1_driver.c"
  */
extern lpbe1_status_t lpbe1_read_reg(uint8_t regaddr,uint8_t *buf);
extern lpbe1_status_t lbe1_write_reg(uint8_t regaddr,uint8_t *buf);
extern lpbe1_status_t lpbe1_read_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len);
extern lpbe1_status_t lpbe1_write_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len);

 
/**
  *@brief: Get system time stamp
  *@para: Pointer to float variable use to save read value
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_timestamp(uint32_t *time)
{
    uint8_t buffer[4];
    if(lpbe1_read_buffer(TIMESTAMP_0, buffer, 4) == LPBE1_OK)
    {
        *time = uint8_to_uint32(buffer);
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get accelerometer data
  *@para: Pointer to float array that have 3 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_acc(float *acc)
{
    DataDecoder data[3];
    if(lpbe1_read_buffer(ACC_X_0, (uint8_t *)data[0].u8vals, 12) == LPBE1_OK)
    {
        for(uint8_t i = 0; i<3; i++)
        {
            *(acc+i) = data[i].fval;
        }
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;   
}


/**
  *@brief: Get gyroscope data
  *@para: Pointer to float array that have 3 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_gyr(float *gyr)
{
    DataDecoder data[3];
    if(lpbe1_read_buffer(GYR_X_0, (uint8_t *)data[0].u8vals, 12) == LPBE1_OK)
    {
        for(uint8_t i = 0; i<3; i++)
        {
            *(gyr+i) = data[i].fval;
        }
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get euler angle
  *@para: Pointer to float array that have 3 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_euler(float *euler)
{
    DataDecoder data[3];
    if(lpbe1_read_buffer(EULER_X_0, (uint8_t *)data[0].u8vals, 12) == LPBE1_OK)
    {
        for(uint8_t i = 0; i<3; i++)
        {
            *(euler+i) = data[i].fval;
        }
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get quateration
  *@para: Pointer to float array that have 4 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_quat(float *quat)
{
    DataDecoder data[4];
    if(lpbe1_read_buffer(QUAT_W_0, (uint8_t *)data[0].u8vals, 16) == LPBE1_OK)
    {
        for(uint8_t i = 0; i<4; i++)
        {
            *(quat+i) = data[i].fval;
        }
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get liner  acceleration
  *@para: Pointer to float array that have 3 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_linacc(float *linacc)
{
    DataDecoder data[3];
    if(lpbe1_read_buffer(LIN_ACC_X_0, (uint8_t *)data[0].u8vals, 12) == LPBE1_OK)
    {
        for(uint8_t i = 0; i<3; i++)
        {
            *(linacc+i) = data[i].fval;
        }
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get hardware system temperature
  *@para: Pointer to float array that have 3 elements at least
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_temp(float *temp)
{
    uint8_t buffer[4];
    if(lpbe1_read_buffer(TEMP_0, buffer, 4) == LPBE1_OK)
    {
        *temp = uint8_to_float(buffer);
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Get all data of lpms-be1
  *@para: Pointer to Lpbe1_Data structure
  *@ret: Status, return LPBE1_OK if read success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_get_all(Lpbe1_Data *alldata)
{
    uint8_t buffer[LPBE1_DATA_SIZE];
    Lpbe1_Data *pall;
    if(lpbe1_read_buffer(DATA_BASE, buffer, LPBE1_DATA_SIZE) == LPBE1_OK)
    {
        pall = (Lpbe1_Data *)&buffer[0];

        *alldata = *pall;

        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}




/**************************************************************************************/

/**
  *@brief: Config registers write enable
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_config_enable(void)
{
    uint8_t data = 0x80; 
    if(lpbe1_write_reg(FUN_CONFIG, &data) == LPBE1_OK)
    {
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Config registers write disable
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_config_disable(void)
{
    uint8_t data = 0x00;
    if(lpbe1_write_reg(FUN_CONFIG, &data) == LPBE1_OK)
    {
        return LPBE1_OK;
    }
    else
        return LPBE1_ERROR;
}


/**
  *@brief: Reset all registers
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_sys_reset(void)
{
    uint8_t data;
    if(lpbe1_read_reg(SYS_CONFIG, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_SYS_RESET;
    
    if(lpbe1_write_reg(SYS_CONFIG, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Reboot the Sensor
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_sys_reboot(void)
{
    uint8_t data;
    if(lpbe1_read_reg(SYS_CONFIG, &data) != LPBE1_OK) 
        return LPBE1_ERROR;

    data |= LPMS_SYS_REBOOT;

    if(lpbe1_write_reg(SYS_CONFIG, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Reset the timestamp
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_reset_time(void)
{
    uint8_t data;
    if(lpbe1_read_reg(DATA_CTRL, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_RESET_TIME;
    
    if(lpbe1_write_reg(DATA_CTRL, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Set the gyr to output in radians
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_radianOutput(void)
{
    uint8_t data;
    if(lpbe1_read_reg(DATA_CTRL, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_RADIAN_OUTPUT;

    if(lpbe1_write_reg(DATA_CTRL, &data) != LPBE1_OK)
        return LPBE1_ERROR;

    return LPBE1_OK;

}


/**
  *@brief: Set output data frequency
  *@para: frequency for the the output data. 
    This parameter can be one of the following values:
        LPMS_DATA_FREQ_5HZ
        LPMS_DATA_FREQ_10HZ
        LPMS_DATA_FREQ_50HZ
        LPMS_DATA_FREQ_100HZ
        LPMS_DATA_FREQ_250HZ
        LPMS_DATA_FREQ_500HZ
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_Freq(uint8_t freq)
{
    uint8_t data;
    if(lpbe1_read_reg(DATA_CTRL, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data &= ~LPMS_DATA_FREQ_MASK;
    
    if(freq == LPMS_DATA_FREQ_5HZ)          data |= LPMS_DATA_FREQ_5HZ;
    else if(freq == LPMS_DATA_FREQ_10HZ)    data |= LPMS_DATA_FREQ_10HZ;
    else if(freq == LPMS_DATA_FREQ_50HZ)    data |= LPMS_DATA_FREQ_50HZ;
    else if(freq == LPMS_DATA_FREQ_100HZ)   data |= LPMS_DATA_FREQ_100HZ;
    else if(freq == LPMS_DATA_FREQ_250HZ)   data |= LPMS_DATA_FREQ_250HZ;
    else if(freq == LPMS_DATA_FREQ_500HZ)   data |= LPMS_DATA_FREQ_500HZ;
    
    if(lpbe1_write_reg(DATA_CTRL, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Set output data enable
  *@para: Select which data output enable
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_DataOutput(uint8_t data)
{
    if(lpbe1_write_reg(DATA_ENABLE, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Set ACC range
  *@para: Range for the acc
    This parameter can be one of the following values:
        LPMS_ACC_FS_2G
        LPMS_ACC_FS_4G
        LPMS_ACC_FS_8G
        LPMS_ACC_FS_16G
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_AccRange(uint8_t range)
{
    uint8_t data;
    if(lpbe1_read_reg(CTRL_0_A, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data &= ~LPMS_ACC_FS_MASK;
    
    if(range == LPMS_ACC_FS_2G)         data |= LPMS_ACC_FS_2G;
    else if(range == LPMS_ACC_FS_4G)    data |= LPMS_ACC_FS_4G;
    else if(range == LPMS_ACC_FS_8G)    data |= LPMS_ACC_FS_8G;
    else if(range == LPMS_ACC_FS_16G)   data |= LPMS_ACC_FS_16G;
    
    if(lpbe1_write_reg(CTRL_0_A, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Set GYR range
  *@para: Range for the gyr
    This parameter can be one of the following values:
        LPMS_GYR_FS_125DPS
        LPMS_GYR_FS_250DPS
        LPMS_GYR_FS_500DPS
        LPMS_GYR_FS_1000DPS
        LPMS_GYR_FS_2000DPS
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_GyrRange(uint8_t range)
{
    uint8_t data;
    if(lpbe1_read_reg(CTRL_1_G, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data &= ~LPMS_GYR_FS_MASK;
    
    if(range == LPMS_GYR_FS_125DPS)         data |= LPMS_GYR_FS_125DPS;
    else if(range == LPMS_GYR_FS_250DPS)    data |= LPMS_GYR_FS_250DPS;
    else if(range == LPMS_GYR_FS_500DPS)    data |= LPMS_GYR_FS_500DPS;
    else if(range == LPMS_GYR_FS_1000DPS)   data |= LPMS_GYR_FS_1000DPS;
    else if(range == LPMS_GYR_FS_2000DPS)   data |= LPMS_GYR_FS_2000DPS;
    
    if(lpbe1_write_reg(CTRL_1_G, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Set gyr output type
  *@para: Type for the gyr
    This parameter can be one of the following values:
        LPMS_GYR_OUTPUT_RAW
        LPMS_GYR_OUTPUT_BIAS_CALIBRATED
        LPMS_GYR_OUTPUT_ALIGNMENT_CALIBRATED
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_set_Gyroutputtype(uint8_t type)
{
    uint8_t data;
    if(lpbe1_read_reg(CTRL_1_G, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
     data &= ~LPMS_GYR_OUTPUT_MASK;
    if(type == LPMS_GYR_OUTPUT_RAW)                         
        data |= LPMS_GYR_OUTPUT_RAW;
    else if(type == LPMS_GYR_OUTPUT_BIAS_CALIBRATED)        
        data |= LPMS_GYR_OUTPUT_BIAS_CALIBRATED;
    else if(type == LPMS_GYR_OUTPUT_ALIGNMENT_CALIBRATED)  
        data |= LPMS_GYR_OUTPUT_ALIGNMENT_CALIBRATED;
 
    if(lpbe1_write_reg(CTRL_1_G, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Start GYR Calibration
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_start_GyrCalibra(void)
{
    uint8_t data;
    if(lpbe1_read_reg(CTRL_1_G, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_GYR_CALIBRA;

    if(lpbe1_write_reg(CTRL_1_G, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Reset the offset
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_reset_offset(void)
{
    uint8_t data;
    if(lpbe1_read_reg(OFFSET_SETTING, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_RESET_OFFSET;

    if(lpbe1_write_reg(OFFSET_SETTING, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Heading reset function
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_heading_reset(void)
{
    uint8_t data;
    if(lpbe1_read_reg(OFFSET_SETTING, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_HEADING_RESET;

    if(lpbe1_write_reg(OFFSET_SETTING, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Object reset function
  *@ret: Status, return LPBE1_OK if write success otherwise return LPBE1_ERROR
  */
lpbe1_status_t lpbe1_object_reset(void)
{
    uint8_t data;
    if(lpbe1_read_reg(OFFSET_SETTING, &data) != LPBE1_OK) 
        return LPBE1_ERROR;
    
    data |= LPMS_OBJECT_RESET;

    if(lpbe1_write_reg(OFFSET_SETTING, &data) != LPBE1_OK)
        return LPBE1_ERROR;
    
    return LPBE1_OK;
}


/**
  *@brief: Convert 4 uint8_t values to float value
  *@para: Pointer to uint8_t array[4];
  *@ret: float value
  */
float uint8_to_float(uint8_t *pu8vals)
{
    DataDecoder decoder;
    for(uint8_t i = 0; i < 4; i++)
    {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.fval;
}


/**
  *@brief: Convert 4 uint8_t values to uint32 value
  *@para: Pointer to uint8_t array[4];
  *@ret: float value
  */
uint32_t uint8_to_uint32(uint8_t *pu8vals)
{
    DataDecoder decoder;
    for(uint8_t i = 0; i < 4; i++)
    {
        decoder.u8vals[i] = *(pu8vals +i);
    }
    return decoder.u32val;
}


/**
  *@brief: Convert uint32_t values to float value
  *@para: Pointer to uint32_t variable
  *@ret: float value
  */
float uint32_to_float(uint32_t *pu32val)
{
    DataDecoder decoder;

    decoder.u32val= *pu32val;

    return decoder.fval;

}


#ifdef EXAMPLE_DEBUG
/* lpbe1_example
  * an example function to show how to use this library
  */
void lpbe1_example(void)
{
    float timedata;
    Lpbe1_Data lpbe1Data = { 0 };

    lpbe1_get_timestamp(&lpbe1Data.time);
    lpbe1_get_acc(lpbe1Data.acc);
    lpbe1_get_gyr(lpbe1Data.gyr);
    lpbe1_get_euler(lpbe1Data.euler);
    lpbe1_get_quat(lpbe1Data.quat);
    lpbe1_get_linacc(lpbe1Data.linAcc);
    lpbe1_get_temp(&lpbe1Data.temp);
    // lpbe1_get_all(&lpbe1Data);
    
    timedata = lpbe1Data.time*0.002;
    printf("Timestamp: %.3f \r\n", timedata);
    printf("AccX: %.3f\tAccY: %.3f\tAccZ: %.3f\t \r\n", lpbe1Data.acc[0],lpbe1Data.acc[1],lpbe1Data.acc[2]);
    printf("GyrX: %.3f\tGyrY: %.3f\tGyrZ: %.3f\t \r\n", lpbe1Data.gyr[0],lpbe1Data.gyr[1],lpbe1Data.gyr[2]);
    printf("EulerX: %f\tEulerY: %f\tEulerZ: %f\t \r\n", lpbe1Data.euler[0],lpbe1Data.euler[1],lpbe1Data.euler[2]);
    printf("QuatW: %f\tQuatX: %f\tQuatY: %f\tQuatZ: %f\t  \r\n",lpbe1Data.quat[0],lpbe1Data.quat[1],lpbe1Data.quat[2],lpbe1Data.quat[3]);
    printf("Temperature: %.1f \r\n",lpbe1Data.temp);
    printf("\r\n");


}


void lpbe1_config_example(void)
{
    lpbe1_config_enable();      //registers write enable
    HAL_Delay(2);               //wait for the sensor to update the register, delay 2.5ms or longer
    
    /*
    lpbe1_sys_reboot();         //rebooot the sensor, all registers reset to default value.
    HAL_Delay(80);              //when reboot the sensor, delay 200ms or longer
    
    lpbe1_config_enable();      //registers write enable
    HAL_Delay(2);

    lpbe1_sys_reset();          //Reset all the registers
    HAL_Delay(10);              //when updata all the config registers in one time, delay about 20ms or longer

    lpbe1_config_enable();      //registers write enable
    HAL_Delay(2);
    */

    lpbe1_set_DataOutput(LPMS_ALLDATA_EN);
    HAL_Delay(2);
    
    lpbe1_set_Freq(LPMS_DATA_FREQ_100HZ);
    HAL_Delay(2);
    
    lpbe1_set_AccRange(LPMS_ACC_FS_4G);
    HAL_Delay(2);
    
    //Other register configuration, refer to the Functions in this file
    
    lpbe1_config_disable();    //registers write disable
    HAL_Delay(2);
    
    lpbe1_config_disable();    //registers write disable
        
}

#endif/*DEBUG*/


#ifdef __cplusplus
}
#endif /* __cplusplus */
