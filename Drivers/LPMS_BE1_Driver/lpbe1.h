/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/
#ifndef __LPBE1_H
#define __LPBE1_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "stdio.h"
#include "stdint.h"


/*
  *System Registers{
  */
#define LPBE1_ALL_REG_SIZE                      128
#define LPBE1_DATA_SIZE                         84

//Data register start address
#define DATA_BASE                               0x20
#define LPMS_INFO_BASE                          0x74

#define STATUS                                  0x00
#define FUN_CONFIG                              0x01
#define SYS_CONFIG                              0x02
#define DATA_CTRL                               0x03
#define DATA_ENABLE                             0x04
#define CTRL_0_A                                0x05
#define CTRL_1_G                                0x06
#define FILTER_CONFIG                           0x07
#define OFFSET_SETTING                          0x08

//Data Register
#define TIMESTAMP_0                             (DATA_BASE + 0)
#define TIMESTAMP_1                             (DATA_BASE + 1)
#define TIMESTAMP_2                             (DATA_BASE + 2)
#define TIMESTAMP_3                             (DATA_BASE + 3)
#define ACC_X_0                                 (DATA_BASE + 4)
#define ACC_X_1                                 (DATA_BASE + 5)
#define ACC_X_2                                 (DATA_BASE + 6)
#define ACC_X_3                                 (DATA_BASE + 7)
#define ACC_Y_0                                 (DATA_BASE + 8)
#define ACC_Y_1                                 (DATA_BASE + 9)
#define ACC_Y_2                                 (DATA_BASE + 10)
#define ACC_Y_3                                 (DATA_BASE + 11)
#define ACC_Z_0                                 (DATA_BASE + 12)
#define ACC_Z_1                                 (DATA_BASE + 13)
#define ACC_Z_2                                 (DATA_BASE + 14)
#define ACC_Z_3                                 (DATA_BASE + 15)
#define GYR_X_0                                 (DATA_BASE + 16)
#define GYR_X_1                                 (DATA_BASE + 17)
#define GYR_X_2                                 (DATA_BASE + 18)
#define GYR_X_3                                 (DATA_BASE + 19)
#define GYR_Y_0                                 (DATA_BASE + 20)
#define GYR_Y_1                                 (DATA_BASE + 21)
#define GYR_Y_2                                 (DATA_BASE + 22)
#define GYR_Y_3                                 (DATA_BASE + 23)
#define GYR_Z_0                                 (DATA_BASE + 24)
#define GYR_Z_1                                 (DATA_BASE + 25)
#define GYR_Z_2                                 (DATA_BASE + 26)
#define GYR_Z_3                                 (DATA_BASE + 27)

#define QUAT_W_0                                (DATA_BASE + 40)
#define QUAT_W_1                                (DATA_BASE + 41)
#define QUAT_W_2                                (DATA_BASE + 42)
#define QUAT_W_3                                (DATA_BASE + 43)
#define QUAT_X_0                                (DATA_BASE + 44)
#define QUAT_X_1                                (DATA_BASE + 45)
#define QUAT_X_2                                (DATA_BASE + 46)
#define QUAT_X_3                                (DATA_BASE + 47)
#define QUAT_Y_0                                (DATA_BASE + 48)
#define QUAT_Y_1                                (DATA_BASE + 49)
#define QUAT_Y_2                                (DATA_BASE + 50)
#define QUAT_Y_3                                (DATA_BASE + 51)
#define QUAT_Z_0                                (DATA_BASE + 52)                 
#define QUAT_Z_1                                (DATA_BASE + 53)                 
#define QUAT_Z_2                                (DATA_BASE + 54)                 
#define QUAT_Z_3                                (DATA_BASE + 55)                 
#define EULER_X_0                               (DATA_BASE + 56)
#define EULER_X_1                               (DATA_BASE + 57)
#define EULER_X_2                               (DATA_BASE + 58)
#define EULER_X_3                               (DATA_BASE + 59)
#define EULER_Y_0                               (DATA_BASE + 60)
#define EULER_Y_1                               (DATA_BASE + 61)
#define EULER_Y_2                               (DATA_BASE + 62)
#define EULER_Y_3                               (DATA_BASE + 63)
#define EULER_Z_0                               (DATA_BASE + 64)
#define EULER_Z_1                               (DATA_BASE + 65)
#define EULER_Z_2                               (DATA_BASE + 66)
#define EULER_Z_3                               (DATA_BASE + 67)
#define LIN_ACC_X_0                             (DATA_BASE + 68)
#define LIN_ACC_X_1                             (DATA_BASE + 69)
#define LIN_ACC_X_2                             (DATA_BASE + 70)
#define LIN_ACC_X_3                             (DATA_BASE + 71)
#define LIN_ACC_Y_0                             (DATA_BASE + 72)
#define LIN_ACC_Y_1                             (DATA_BASE + 73)
#define LIN_ACC_Y_2                             (DATA_BASE + 74)
#define LIN_ACC_Y_3                             (DATA_BASE + 75)
#define LIN_ACC_Z_0                             (DATA_BASE + 76)
#define LIN_ACC_Z_1                             (DATA_BASE + 77)
#define LIN_ACC_Z_2                             (DATA_BASE + 78)
#define LIN_ACC_Z_3                             (DATA_BASE + 79)
#define TEMP_0                                  (DATA_BASE + 80)
#define TEMP_1                                  (DATA_BASE + 81)
#define TEMP_2                                  (DATA_BASE + 82)
#define TEMP_3                                  (DATA_BASE + 83)

//System information registers
//#define WHO AM I                              (DATA_BASE + 84)
//#define FIRMWARE_VERSION_0                    (DATA_BASE + 85)
//#define FIRMWARE_VERSION_0                    (DATA_BASE + 86)
/*
  *}System Registers                            
  */

//Status
#define LPMS_DATA_READY                         ((uint8_t)0x01 << 0)
#define LPMS_GYR_IS_CALIBRATING                 ((uint8_t)0x01 << 4)


//Function config
#define LPMS_FUN_CONFIG_MASK                    ((uint8_t)0x80)
#define LPMS_FUN_CFG_EN                         ((uint8_t)0x01 << 7)


//System config
#define LPMS_SYS_CONFIG_MASK                    ((uint8_t)0xC0)
#define LPMS_SYS_REBOOT                         ((uint8_t)0x01 << 6)
#define LPMS_SYS_RESET                          ((uint8_t)0x01 << 7)


//Data control
#define LPMS_DATA_CTRL_MASK                     ((uint8_t)0x6F)

#define LPMS_DATA_FREQ_MASK                     ((uint8_t)0x0F << 0)
#define LPMS_DATA_FREQ_5HZ                      ((uint8_t)0x00 << 0)
#define LPMS_DATA_FREQ_10HZ                     ((uint8_t)0x01 << 0)
#define LPMS_DATA_FREQ_50HZ                     ((uint8_t)0x02 << 0)
#define LPMS_DATA_FREQ_100HZ                    ((uint8_t)0x03 << 0)
#define LPMS_DATA_FREQ_250HZ                    ((uint8_t)0x04 << 0)
#define LPMS_DATA_FREQ_500HZ                    ((uint8_t)0x05 << 0)

#define LPMS_RESET_TIME                         ((uint8_t)0x01 << 5)
#define LPMS_RADIAN_OUTPUT                      ((uint8_t)0x01 << 6)


//Data enable
#define LPMS_DATA_EN_MASK                       ((uint8_t)0xF7)
#define LPMS_ALLDATA_EN                         LPMS_DATA_EN_MASK
#define LPMS_TIME_EN                            ((uint8_t)0x01 << 0)
#define LPMS_ACC_EN                             ((uint8_t)0x01 << 1)
#define LPMS_GYR_EN                             ((uint8_t)0x01 << 2)
#define LPMS_QUAT_EN                            ((uint8_t)0x01 << 4)
#define LPMS_EULER_EN                           ((uint8_t)0x01 << 5)
#define LPMS_LINACC_EN                          ((uint8_t)0x01 << 6)
#define LPMS_TEMP_EN                            ((uint8_t)0x01 << 7)



//CONTROL 0 ACC
#define LPMS_ACC_MASK                           ((uint8_t)0x1C)

#define LPMS_ACC_FS_MASK                        ((uint8_t)0x03 << 2)
#define LPMS_ACC_FS_2G                          ((uint8_t)0x00 << 2)
#define LPMS_ACC_FS_4G                          ((uint8_t)0x01 << 2)
#define LPMS_ACC_FS_8G                          ((uint8_t)0x02 << 2)
#define LPMS_ACC_FS_16G                         ((uint8_t)0x03 << 2)

#define LPMS_ACC_OUTPUT_CALIBRATED              ((uint8_t)0x01 << 4)


//CONTROL 1 GYR
#define LPMS_GYR_MASK                           ((uint8_t)0xBE)

#define LPMS_GYR_FS_MASK                        ((uint8_t)0x07 << 1)
#define LPMS_GYR_FS_125DPS                      ((uint8_t)0x00 << 1)
#define LPMS_GYR_FS_250DPS                      ((uint8_t)0x01 << 1)
#define LPMS_GYR_FS_500DPS                      ((uint8_t)0x02 << 1)
#define LPMS_GYR_FS_1000DPS                     ((uint8_t)0x03 << 1)
#define LPMS_GYR_FS_2000DPS                     ((uint8_t)0x04 << 1)

#define LPMS_GYR_OUTPUT_MASK                    ((uint8_t)0x03 << 4)
#define LPMS_GYR_OUTPUT_RAW                     ((uint8_t)0x00 << 4)
#define LPMS_GYR_OUTPUT_BIAS_CALIBRATED         ((uint8_t)0x01 << 4)
#define LPMS_GYR_OUTPUT_ALIGNMENT_CALIBRATED    ((uint8_t)0x02 << 4)

#define LPMS_GYR_CALIBRA                        ((uint8_t)0x01 << 7)


//Filter config
#define LPMS_FILTER_CINFIG_MASK                 ((uint8_t)0x3F)

#define LPMS_FILTER_MODE_MASK                   ((uint8_t)0x07 << 0)
#define LPMS_FILTER_MODE_GYR                    ((uint8_t)0x00 << 0)
#define LPMS_FILTER_MODE_KALMAN_GYR_ACC         ((uint8_t)0x01 << 0)
#define LPMS_FILTER_MODE_DCM_GYR_ACC            ((uint8_t)0x03 << 0)


//Offset Setting
#define LPMS_OFFSET_SETTING_MASK                ((uint8_t)0x0F)
#define LPMS_OBJECT_RESET                       ((uint8_t)0x01 << 0)
#define LPMS_HEADING_RESET                      ((uint8_t)0x01 << 1)
#define LPMS_ALIGNMENT_RESET                    ((uint8_t)0x01 << 2)
#define LPMS_RESET_OFFSET                       ((uint8_t)0x01 << 3)

/*
 *  } Register define
 */
    
 
    
    
    
typedef union
{
    uint8_t u8vals[4];
    uint32_t u32val;
    float fval;
} DataDecoder;

typedef enum
{
    LPBE1_ERROR = 0,
    LPBE1_OK = 1
} lpbe1_status_t;

typedef struct
{
    uint32_t time;
    float acc[3];
    float gyr[3];
    float euler[3];
    float quat[4];
    float linAcc[3];
    float temp;
} Lpbe1_Data;


lpbe1_status_t lpbe1_get_timestamp(uint32_t *time);
lpbe1_status_t lpbe1_get_acc(float *acc);
lpbe1_status_t lpbe1_get_gyr(float *gyr);
lpbe1_status_t lpbe1_get_euler(float *euler);
lpbe1_status_t lpbe1_get_quat(float *quat);
lpbe1_status_t lpbe1_get_linacc(float *linacc);
lpbe1_status_t lpbe1_get_temp(float *temp);
lpbe1_status_t lpbe1_get_all(Lpbe1_Data *alldata);


/*
  *Register config Functions{
  */
lpbe1_status_t lpbe1_config_enable(void);
lpbe1_status_t lpbe1_config_disable(void);
lpbe1_status_t lpbe1_sys_reset(void);
lpbe1_status_t lpbe1_sys_reboot(void);
lpbe1_status_t lpbe1_reset_time(void);
lpbe1_status_t lpbe1_set_radianOutput(void);
lpbe1_status_t lpbe1_set_Freq(uint8_t freq);
lpbe1_status_t lpbe1_set_DataOutput(uint8_t data);
lpbe1_status_t lpbe1_set_AccRange(uint8_t range);
lpbe1_status_t lpbe1_set_GyrRange(uint8_t range);
lpbe1_status_t lpbe1_set_Gyroutputtype(uint8_t type);
lpbe1_status_t lpbe1_start_GyrCalibra(void);
lpbe1_status_t lpbe1_reset_offset(void);
lpbe1_status_t lpbe1_heading_reset(void);
lpbe1_status_t lpbe1_object_reset(void);

float uint8_to_float(uint8_t *pu8vals);
uint32_t uint8_to_uint32(uint8_t *pu8vals);
float uint32_to_float(uint32_t *pu32val);


#define EXAMPLE_DEBUG
#ifdef EXAMPLE_DEBUG
void lpbe1_example(void);
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif/*__LPBE1_H*/
