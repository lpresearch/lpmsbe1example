/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#ifndef __LPBE1_DRIVER_H
#define __LPBE1_DRIVER_H

#include "lpbe1.h"
#include "stm32f4xx_hal.h"

#ifdef USE_I2C_INTERFACE
    #include "i2c.h"
#elif defined USE_SPI_INTERFACE
    #include "spi.h"
#endif

#ifdef USE_I2C_INTERFACE
    #define LPBE1_I2C_ADRRESS  0x64     // MODE1 = 1; MODE0 = 0;
    #define LPBE1_HI2C hi2c1
#elif defined USE_SPI_INTERFACE
    #define LPBE1_HSPI hspi1
#endif


#define LPBE1_MAX_TIMEOUT 100

lpbe1_status_t lpbe1_read_reg(uint8_t regaddr,uint8_t *buf);
lpbe1_status_t lpbe1_write_reg(uint8_t regaddr,uint8_t *buf);
lpbe1_status_t lpbe1_read_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len);
lpbe1_status_t lpbe1_write_buffer(uint8_t regaddr,uint8_t *buf,uint8_t len);


#endif/*__LPBE1_H*/
