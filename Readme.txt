    
    Drvier and example for lpms-be1 motion sensor
    
Files:
    /lpbe1/ : Drvier for lpms-be1 motion sensor
    /stm32f401re/: Example project based on stm32,
                   test on STM32F401RE NUCLEO and can be easily tailored to any other stm32 devices.
    
    
   
    
    Copyright (C) 2020 LP-Research
    All rights reserved.
    Contact: LP-Research (info@lp-research.com)